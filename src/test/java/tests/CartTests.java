package tests;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CartTests extends BaseTest{

    private static final String SIZE = "38";
    private static final String EXPECTED_AMOUNT_IN_BAG_WITH_ONE_ITEM = "1";
    private static final String EXPECTED_AMOUNT_IN_BAG_WITH_TWO_ITEMS = "2";

    @Test(priority = 9)
    public void addProductToCart() {
        getHomePage().clickWomenCategoryLink();
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickGardenPartyLink();
        getBasePage().waitForPageReadyState(30);
        getGardenPartyPage().clickMiniSmockSundressProductLink();
        getBasePage().waitForPageReadyState(30);
        getGardenPartyProductPage().selectSize(SIZE);
        getGardenPartyProductPage().clickAddToBag();

        getBasePage().implicitlyWait(15);
        assertEquals(EXPECTED_AMOUNT_IN_BAG_WITH_ONE_ITEM, getGardenPartyProductPage().getAmountOfItemsInTheBag());
    }

    @Test(priority = 10)
    public void deleteProductFromCart() {
        getHomePage().clickWomenCategoryLink();
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickMainNavOutlet();
        getBasePage().waitForElementVisibility(30, getHomePage().getOutletDropdownMenu());
        getHomePage().clickBagsAndPursesLinkInOutletMenu();
        getBasePage().waitForPageReadyState(30);
        getOutletBagsAndPursePage().clickBackpackBlackProduct();
        getBasePage().waitForPageReadyState(30);
        getOutletBagsAndPurseBackpackPage().clickAddToBag();
        getBasePage().implicitlyWait(15);

        getHomePage().clickWomenCategoryLink();
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickGardenPartyLink();
        getBasePage().waitForPageReadyState(30);
        getGardenPartyPage().clickMiniSmockSundressProductLink();
        getBasePage().waitForPageReadyState(30);
        getGardenPartyProductPage().selectSize(SIZE);
        getGardenPartyProductPage().clickAddToBag();
        getGardenPartyProductPage().waitForElementDisapearing(30, getGardenPartyProductPage().getItemsInBagPath(), EXPECTED_AMOUNT_IN_BAG_WITH_TWO_ITEMS);
        assertEquals(EXPECTED_AMOUNT_IN_BAG_WITH_TWO_ITEMS, getGardenPartyProductPage().getAmountOfItemsInTheBag());

        getGardenPartyProductPage().clickDeleteItemFromBag();
        getGardenPartyProductPage().waitForElementDisapearing(30, getGardenPartyProductPage().getItemsInBagPath(), EXPECTED_AMOUNT_IN_BAG_WITH_ONE_ITEM);
        assertEquals(EXPECTED_AMOUNT_IN_BAG_WITH_ONE_ITEM, getGardenPartyProductPage().getAmountOfItemsInTheBag());
    }
}
