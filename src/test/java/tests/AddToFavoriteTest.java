package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class AddToFavoriteTest extends BaseTest{

    private static final int EXPECTED_AMOUNT_IN_SAVED_LIST = 1;

    @Test(priority = 8)
    public void checkThatTheProductIsAddedToFavorites() {
        getHomePage().clickWomenCategoryLink();
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickMainNavOutlet();
        getBasePage().waitForElementVisibility(30, getHomePage().getOutletDropdownMenu());
        getHomePage().clickBagsAndPursesLinkInOutletMenu();
        getBasePage().waitForPageReadyState(30);
        getOutletBagsAndPursePage().clickBackpackBlackProduct();
        getBasePage().waitForPageReadyState(30);
        getOutletBagsAndPurseBackpackPage().clickAddToFavoriteButton();
        getHomePage().clickFavoritesButton();
        getBasePage().waitForElementVisibility(30, getSavedItems().getSavedItemsSection());
        List<WebElement> favoriteProductsList = getSavedItems().getFavoriteProductsList();
        assertEquals(favoriteProductsList.size(), EXPECTED_AMOUNT_IN_SAVED_LIST);
    }
}
