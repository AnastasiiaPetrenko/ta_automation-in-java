package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

public class SignInTest extends BaseTest{

    private static final int EXPECTED_ERROR_MESSAGES = 2;

    private static final String EMAIL = "testing@gmail.com";
    private static final String PASSWORD = "test";
    
    @Test(priority = 5)
    public void checkTheSignInFormWithEmptyFields() {
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickAccountIconLink();
        getBasePage().waitForElementVisibility(30, getLoginPage().getAccountDropdownList() );
        getHomePage().clickSignInLink();
        getBasePage().waitForPageReadyState(30);
        getLoginPage().clickSignInButton();
        getBasePage().waitForElementVisibility(30, getLoginPage().getErrorMessage());
        List<WebElement> errorList = getLoginPage().getErrorList();
        assertEquals(EXPECTED_ERROR_MESSAGES, errorList.size());
    }

    @Test (priority = 6)
    public void checkTheSignInFormWithValidInputAndUnexistedUser() {
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickAccountIconLink();
        getBasePage().waitForElementVisibility(30, getLoginPage().getAccountDropdownList() );
        getHomePage().clickSignInLink();
        getBasePage().waitForPageReadyState(30);
        getLoginPage().enterCredentials(EMAIL, PASSWORD);
        getLoginPage().clickSignInButton();
        assertTrue(getLoginPage().getElement().isDisplayed());
    }

}
