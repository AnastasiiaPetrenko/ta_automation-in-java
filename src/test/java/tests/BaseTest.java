package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.*;

public class BaseTest {
    private WebDriver driver;

    private static final String CHROME_DRIVER = "webdriver.chrome.driver";
    private static final String SOURCE = "src\\main\\resources\\chromedriver.exe";
    private static final String ASOS_URL = "https://www.asos.com/";

    public WebDriver getDriver() {
        return driver;
    }

    public BasePage getBasePage() {
        return new BasePage(driver);
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(driver);
    }

    public GardenPartyPage getGardenPartyPage() {
        return new GardenPartyPage(driver);
    }

    public GardenPartyProductPage getGardenPartyProductPage() {
        return new GardenPartyProductPage(driver);
    }

    public OutletBagsAndPursesPage getOutletBagsAndPursePage() {
        return new OutletBagsAndPursesPage(driver);
    }

    public OutletBagsAndPursesBackpackPage getOutletBagsAndPurseBackpackPage() {
        return new OutletBagsAndPursesBackpackPage(driver);
    }

    public LoginPage getLoginPage() {
        return new LoginPage(driver);
    }

    public SavedItems getSavedItems() {
        return new SavedItems(driver);
    }

    @BeforeTest
    public void profileSetUp() {
        System.setProperty(CHROME_DRIVER, SOURCE);
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(ASOS_URL);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }
}
