package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class FitAssistantSizeTest extends BaseTest {

    private static final String HEIGHT = "180";
    private static final String WEIGHT = "70";
    private static final String BUST = "85";
    private static final String CUP = "B";
    private static final String AGE = "20";
    private static final String BRAND = "Mango";
    private static final String BRAND_SIZE = "M";
    private static final String EXPECTED_SIZE = "38";

    @Test(priority = 7)
    public void SelectSizeWithAssistant() {
        getHomePage().clickWomenCategoryLink();
        getBasePage().waitForPageReadyState(30);
        getHomePage().clickGardenPartyLink();
        getBasePage().waitForPageReadyState(30);
        getGardenPartyPage().clickMiniSmockSundressProductLink();
        getBasePage().waitForPageReadyState(30);
        getBasePage().waitForElementVisibility(60, getGardenPartyProductPage().getAssistantLink());
        getGardenPartyProductPage().clickFitAssistantLink();

        getBasePage().waitForElementVisibility(60, getGardenPartyProductPage().getAssistantPopUp());
        getGardenPartyProductPage().enterHeightAndWeight(HEIGHT, WEIGHT);
        getGardenPartyProductPage().clickContinueButton();

        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getTummyShape());
        getGardenPartyProductPage().clickContinueButton();

        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getHipShape());
        getGardenPartyProductPage().clickContinueButton();

        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getBraSize());
        getGardenPartyProductPage().selectBustSizeElement(BUST);
        getGardenPartyProductPage().selectCupSizeElement(CUP);
        getGardenPartyProductPage().clickContinueButtonOnBraSizeSection();

        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getHowOldAreYou());
        getGardenPartyProductPage().enterAge(AGE);
        getGardenPartyProductPage().clickContinueButton();

        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getFitPreference());
        getGardenPartyProductPage().clickContinueButton();

        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getWhatDoYouWear());
        getGardenPartyProductPage().selectBrand(BRAND);
        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getBrandSizeDropdownList());
        getGardenPartyProductPage().clickBrandSizeTableLink();
        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getSizeTable());
        getGardenPartyProductPage().selectLetterBrandSize(BRAND_SIZE);
        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getBrandSizeButton());
        getGardenPartyProductPage().clickContinueButtonOnBrandSizeSection();
        getBasePage().waitForElementVisibility(30, getGardenPartyProductPage().getFitAssistant());
        assertTrue(getGardenPartyProductPage().getTextFromTheFitAssistant().contains(EXPECTED_SIZE));
    }
}
