package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

public class SearchTests extends BaseTest{

    private static final String QUERY_CORRECT_KEYWORD = "Revolution Skincare";
    private static final int EXPECTED_AMOUNT_ITEMS =  1;

    private static final String QUERY_WRONG_KEYWORD = "hngvf";
    private static final String RESULT_WRONG_KEYWORD = "NOTHING MATCHES YOUR SEARCH";

    private static final String QUERY_SEARCH_KEYWORD = "Jewellery";
    private static final int EXPECTED_MAX_AMOUNT_ITEMS_ON_PAGE =  72;

    private static final String QUERY_KEYWORD = "Revolution";
    private static final String EXPECTED_URL = "https://www.asos.com/";

    @Test (priority = 1)
    public void checkThatCategoryFilterWorksCorrect() {
        getHomePage().searchByKeyword(QUERY_CORRECT_KEYWORD);
        getBasePage().waitForPageReadyState(30);
        getSearchResultsPage().applyFilterCategoryBodyCareAndCloseFilterList();
        getBasePage().implicitlyWait(15);
        List<WebElement> itemList = getSearchResultsPage().getItemsList();
        assertEquals(itemList.size(), EXPECTED_AMOUNT_ITEMS);
    }

    @Test (priority = 2)
    public void checkThatNoItemsFoundForWrongKeyword() {
        getHomePage().searchByKeyword(QUERY_WRONG_KEYWORD);
        getBasePage().waitForPageReadyState(30);
        String text = getSearchResultsPage().getTextFromTheResultSectionOnResultPage();
        assertTrue(text.contains(RESULT_WRONG_KEYWORD));
    }

    @Test (priority = 3)
    public void checkMaxAmountOfItemsAfterSearch () {
        getHomePage().searchByKeyword(QUERY_SEARCH_KEYWORD);
        getBasePage().waitForPageReadyState(30);
        List<WebElement> itemList = getSearchResultsPage().getItemsList();
        assertEquals(EXPECTED_MAX_AMOUNT_ITEMS_ON_PAGE, itemList.size());
    }

    @Test(priority = 4)
    public void checkThatLinkLogoWorksCorrectly() {
        getBasePage().waitForPageReadyState(30);
        getHomePage().searchByKeyword(QUERY_KEYWORD);
        getBasePage().waitForElementVisibility(30,getSearchResultsPage().getPathResultSearch());
        getHomePage().clickMainLogo();
        getBasePage().waitForPageReadyState(30);
        assertEquals(EXPECTED_URL, getDriver().getCurrentUrl());
    }
}