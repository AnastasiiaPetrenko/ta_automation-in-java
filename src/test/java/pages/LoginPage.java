package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginPage extends BasePage{

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement signInButton;

    @FindBy(xpath = "//input[@name='Username']")
    private WebElement usernameInput;

    @FindBy(xpath = "//input[@name='Password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//div[@class='mobile-spacer']")
    private WebElement errorUnmatchedCredentials;

    @FindBy(xpath = "//div[@data-testid='myaccount-dropdown']")
    private By accountDropdownList;

    @FindBy(xpath = "//span[@id='EmailAddress-error']")
    private By errorMessage;

    private final By errorList = By.xpath("//span[contains(@id, '-error')]");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void clickSignInButton() {
        signInButton.click();
    }

    public List<WebElement> getErrorList() {
        return driver.findElements(errorList);
    }

    public void enterCredentials(String name, String pass) {
        usernameInput.sendKeys(name);
        passwordInput.sendKeys(pass);
    }

    public WebElement getElement() {
        return errorUnmatchedCredentials;
    }

    public By getAccountDropdownList() {
        return accountDropdownList;
    }

    public By getErrorMessage() {
        return errorMessage;
    }
}
