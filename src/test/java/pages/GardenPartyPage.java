package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GardenPartyPage extends BasePage {

    @FindBy (xpath = "//article[@id = 'product-14916057']/a")
    private WebElement miniSmockSundressProductLink;

    public GardenPartyPage(WebDriver driver) {
        super(driver);
    }

    public void clickMiniSmockSundressProductLink() {
        miniSmockSundressProductLink.click();
    }
}
