package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SavedItems extends BasePage {

    @FindBy(xpath = "//section[@class='productTiles_2GifC']")
    private By savedItemsSection;

    @FindBy(xpath = "//section[@class='productTiles_2GifC']//li")
    private By savedProductsList;

    public SavedItems (WebDriver driver) {
        super(driver);
    }

    public By getSavedItemsSection() {
        return savedItemsSection;
    }

    public List<WebElement> getFavoriteProductsList() {
        return driver.findElements(savedProductsList);
    }
}
