package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OutletBagsAndPursesPage extends BasePage {

    @FindBy(xpath = "//a[contains(@aria-label, 'CK One')]")
    private WebElement ckBackpackBlackProduct;

    public OutletBagsAndPursesPage(WebDriver driver) {
        super(driver);
    }

    public void clickBackpackBlackProduct() {
        ckBackpackBlackProduct.click();
    }
}
