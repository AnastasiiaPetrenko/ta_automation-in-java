package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GardenPartyProductPage extends BasePage {

    @FindBy(xpath ="//a[contains(@data-bind, 'fit')]")
    private WebElement fitAssistantLink;

    @FindBy(xpath = "//input[@id='uclw_form_height']")
    private WebElement heightInput;

    @FindBy(xpath = "//input[@id='uclw_form_weight']")
    private WebElement weightInput;

    @FindBy(xpath = "//input[@aria-describedby='uclw_age_tooltip']")
    private WebElement ageInput;

    @FindBy(xpath = "//div[@data-ref='sizeTablesLink']")
    private WebElement brandSizeTableLink;

    @FindBy(xpath = "//button[@type='submit'][contains(@class, 'uclw_submit')]")
    private WebElement continueButton;

    @FindBy(xpath = "//button[@data-ref='submit'][contains(@class, 'uclw_submit')]")
    private WebElement continueButtonOnBraSizeSection;

    @FindBy(xpath = "//button[@data-ref='submit'][contains(@class, 'uclw_submit')]")
    private WebElement continueButtonOnBrandSizeSection;

    @FindBy(xpath = "//div[@class='uclw_size']")
    private WebElement assistantSize;

    @FindBy(xpath = "//select[@data-id='sizeSelect']")
    private WebElement sizeField;

    @FindBy(xpath = "//a[contains(@data-bind, 'addToBag')]")
    private WebElement addToBagButton;

    @FindBy(xpath = "//a[span[@type='bagFilled']]/span[2]")
    private WebElement itemsInBag;

    @FindBy(xpath = "//div[@class='_2XTT510 jVCq0Wv liqWkbK'][a[img[contains(@src,'mini-smock-sundress')]]]/button")
    private WebElement deleteItemButton;

    private final By FIT_ASSISTANT_LINK = By.xpath("//a[contains(@data-bind, 'fit')]");
    private final By ASSISTANT_POPUP = By.xpath("//div[@class = 'uclw_whitebox_container']");
    private final By TUMMY_SHAPE = By.xpath("//div[contains(text(), 'Your tummy shape')]");
    private final By HIP_SHARE = By.xpath("//div[contains(text(), 'Your hip shape')]");
    private final By BRA_SIZE = By.xpath("//div[contains(text(), 'Add bra size')]");
    private final By HOW_OLD_ARE_YOU = By.xpath("//div[contains(text(), 'How old are you?')]");
    private final By FIT_PREFERENCE = By.xpath("//div[contains(text(), 'Fit preference')]");
    private final By WHAT_DO_YOU_WEAR = By.xpath("//div[@data-ref='brandBlock']/div[contains(text(), 'What do you wear?')]");
    private final By BRAND_SIZE_DROPDOWN_LIST = By.xpath("//div[@id='uclw_size_dropdown']");
    private final By SIZE_TABLE = By.xpath("//div[@id='uclw_size_tables']");
    private final By BRAND_SIZE_BUTTON = By.xpath("//button[@data-ref='submit'][contains(@class, 'uclw_submit')]");
    private final By FIT_ASSISTANT = By.xpath("//h1[contains(text(), 'Fit Assistant')]");

    private final By ITEMS_IN_BAG_PATH = By.xpath ("//a[span[@type='bagFilled']]/span[2]");

    public GardenPartyProductPage(WebDriver driver) {
        super(driver);
    }

    public void clickFitAssistantLink() {
        fitAssistantLink.click();
    }

    public void enterHeightAndWeight(String height, String weight) {
        heightInput.sendKeys(height);
        weightInput.sendKeys(weight);
    }

    public By getAssistantPopUp() {
        return ASSISTANT_POPUP;
    }

    public By getAssistantLink() {
        return FIT_ASSISTANT_LINK;
    }

    public By getTummyShape() {
        return TUMMY_SHAPE;
    }

    public By getHipShape() {
        return HIP_SHARE;
    }

    public By getBraSize() {
        return BRA_SIZE;
    }

    public By getHowOldAreYou() {
        return HOW_OLD_ARE_YOU;
    }

    public By getFitPreference() {
        return FIT_PREFERENCE;
    }

    public By getWhatDoYouWear() {
        return WHAT_DO_YOU_WEAR;
    }

    public By getBrandSizeDropdownList() {
        return BRAND_SIZE_DROPDOWN_LIST;
    }

    public By getSizeTable() {
        return SIZE_TABLE;
    }

    public By getBrandSizeButton() {
        return BRAND_SIZE_BUTTON;
    }

    public By getFitAssistant() {
        return FIT_ASSISTANT;
    }

    public By getItemsInBagPath() {
        return ITEMS_IN_BAG_PATH;
    }

    public void clickContinueButton() {
        continueButton.click();
    }

    public void clickContinueButtonOnBraSizeSection() {
        continueButtonOnBraSizeSection.click();
    }

    public void clickContinueButtonOnBrandSizeSection() {
        continueButtonOnBrandSizeSection.click();
    }

    public void selectBustSizeElement(String size) {
        String path = "//span[contains(text(), " + size + ")]";
        WebElement bustSize = driver.findElement(By.xpath(path));
        bustSize.click();
    }

    public void selectCupSizeElement(String size) {
        String path = "//div[@data-ref = 'cupSection']//span[contains(text(), '" + size + "')]";
        WebElement cupSize = driver.findElement(By.xpath(path));
        cupSize.click();
    }
    public void enterAge(String age) {
        ageInput.sendKeys(age);
    }

    public void selectBrand(String brand) {
        String path = "//span[img[@alt='"+ brand + "']]";
        WebElement brandStore = driver.findElement(By.xpath(path));
        brandStore.click();
    }

    public void clickBrandSizeTableLink() {
        brandSizeTableLink.click();
    }

    public void selectLetterBrandSize(String size) {
        String path = "//ul[@aria-label='Letter sizes']//span[contains(text(), '" + size + "')]";
        WebElement brandSize = driver.findElement(By.xpath(path));
        brandSize.click();
    }

    public String getTextFromTheFitAssistant() {
        return assistantSize.getText();
    }

    public void selectSize(String size) {
        sizeField.click();
        String path = "//select[@data-id='sizeSelect']/option[contains(text(),'" + size + "')]";
        WebElement productSize = driver.findElement(By.xpath(path));
        productSize.click();
    }

    public void clickAddToBag() {
        addToBagButton.click();
    }

    public String getAmountOfItemsInTheBag() {
        return itemsInBag.getText();
    }

    public void clickDeleteItemFromBag() {
        deleteItemButton.click();
    }

    public void waitForElementDisapearing(long timeout, By locator, String value) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.textToBe(locator, value));
    }

}
