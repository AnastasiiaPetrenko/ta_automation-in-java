package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {

    @FindBy(xpath = "//div[@data-auto-id='productList']")
    private By resultSearch;

    @FindBy (xpath = "//li[contains(@data-auto-id, '10993')]//button[@class = '_1om7l06']")
    private WebElement buttonFilterCategory;

    @FindBy (xpath = "//label[contains(@for, '61400')]")
    private WebElement buttonFilterBodyCare;

    @FindBy (xpath = "//section[contains(@class, 'dynamic')]//h2")
    private WebElement textNothingFound;

    public By getPathResultSearch() {
        return resultSearch;
    }

    private final By itemsList = By.xpath("//article[contains(@id, product)]//div/p");

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getItemsList() {
        return driver.findElements(itemsList);
    }

    public void applyFilterCategoryBodyCareAndCloseFilterList() {
        buttonFilterCategory.click();
        buttonFilterBodyCare.click();
        buttonFilterCategory.click();
    }

    public String getTextFromTheResultSectionOnResultPage() {
        return textNothingFound.getText();
    }

}
