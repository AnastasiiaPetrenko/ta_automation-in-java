package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class OutletBagsAndPursesBackpackPage extends BasePage {

    @FindBy(xpath = "//button[@class='save-button']")
    private WebElement addToFavoriteButton;

    @FindBy(xpath = "//a[contains(@data-bind, 'addToBag')]")
    private WebElement addToBagButton;

    @FindBy(xpath = "//section[@class='productTiles_2GifC']//li")
    private By favoriteProductsList;

    public OutletBagsAndPursesBackpackPage(WebDriver driver) {
        super(driver);
    }

    public void clickAddToFavoriteButton() {
        addToFavoriteButton.click();
    }

    public List<WebElement> getFavoriteProductsList() {
        return driver.findElements(favoriteProductsList);
    }

    public void clickAddToBag() {
        addToBagButton.click();
    }


}
