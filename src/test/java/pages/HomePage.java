package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchInput;

    @FindBy(xpath = "//a[@data-testid = 'women-floor']")
    private WebElement womenCategoryLink;

    @FindBy(xpath = "//a[@aria-label='Saved Items']")
    private WebElement favoritesButton;

    @FindBy(xpath = "//a[@data-testid = 'asoslogo']")
    private WebElement mainLogo;

    @FindBy(xpath = "//div[@class='moment__buttons']/a[contains(@href, 'gardenparty')]")
    private WebElement gardenPartyLink;

    @FindBy(xpath = "//div[ul[@data-id='a92844a0-3b5b-41b6-ad85-9ca81f5c24f1']]")
    private By outletDropdownMenu;

    @FindBy(xpath = "//button['Outlet'][@data-id='a92844a0-3b5b-41b6-ad85-9ca81f5c24f1']")
    private WebElement mainNavOutletButton;

    @FindBy(xpath = "//li[@class='_1g1PWkA _2SQx27S']//a[contains(text(), 'Bags & Purses')]")
    private WebElement bagsAndPursesLinkInOutletMenu;

    @FindBy(xpath = "//button[@data-testid='accountIcon']")
    private WebElement accountIconLink;

    @FindBy(xpath = "//a[contains(text(), 'Sign In')]")
    private WebElement signInLink;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void searchByKeyword(String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickGardenPartyLink() {
        gardenPartyLink.click();
    }

    public void clickWomenCategoryLink() {
        womenCategoryLink.click();
    }

    public void clickFavoritesButton() {
        favoritesButton.click();
    }

    public void clickMainLogo() {
        mainLogo.click();
    }

    public void clickMainNavOutlet() {
        mainNavOutletButton.click();
    }

    public void clickBagsAndPursesLinkInOutletMenu() {
        bagsAndPursesLinkInOutletMenu.click();
    }

    public void clickAccountIconLink() {
        accountIconLink.click();
    }

    public void clickSignInLink() {
        signInLink.click();
    }

    public By getOutletDropdownMenu() {
        return outletDropdownMenu;
    }

}
